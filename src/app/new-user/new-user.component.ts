import { Component, ElementRef, ViewChild } from '@angular/core';
import { UsersService } from '../shared/users.service';
import { User } from '../shared/user.model';

@Component({
  selector: 'app-new-user',
  templateUrl: './new-user.component.html',
  styleUrls: ['./new-user.component.css']
})
export class NewUserComponent {
  @ViewChild('nameInput') nameInput!: ElementRef;
  @ViewChild('emailInput') emailInput!: ElementRef;
  @ViewChild('roleInput') roleInput!: ElementRef;
  @ViewChild('statusInput') statusInput!: ElementRef;
  status: boolean;

  constructor(private userService: UsersService) {
    this.status = false;
  }

  createUser() {
    const name: string = this.nameInput.nativeElement.value;
    const email: string = this.emailInput.nativeElement.value;
    const role: string = this.roleInput.nativeElement.value;
    const user = new User(name, email, role, this.status);

    this.userService.addUser(user);
  }
}

import { EventEmitter } from '@angular/core';
import { Group } from './group.model';
import { User } from './user.model';

export class GroupsService {
  currentGroup: Group[] = [];
  groupsChange = new EventEmitter<Group[]>();

  private groups: Group[] = [];

  getGroups() {
    return this.groups.slice();
  }

  addGroup(group: Group) {
    this.groups.push(group);
    this.groupsChange.emit(this.groups);
  }

  addCurrentGroup(group: Group) {
    this.resetGroupsStatus();
    this.currentGroup = [];
    this.currentGroup.push(group);
    group.status = true;
  }

  resetGroupsStatus() {
    this.groups.forEach(i => {
      i.status = false
    })
  }

  addUserToGroup(user: User) {
    if (this.currentGroup.length != 0) {
      const groupItem = this.groups.find(i => i.name === this.currentGroup[0].name)!;

      const existingItem = groupItem.users.find(cardItem => {
        return cardItem.name === user.name;
      });

      if (!existingItem) {
        groupItem.users.push(user);
      }
    }
  }

}

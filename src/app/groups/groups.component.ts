import { Component, OnInit } from '@angular/core';
import { Group } from '../shared/group.model';
import { GroupsService } from '../shared/groups.service';

@Component({
  selector: 'app-groups',
  templateUrl: './groups.component.html',
  styleUrls: ['./groups.component.css']
})
export class GroupsComponent implements OnInit {
  groups!: Group[];

  constructor(public groupsService: GroupsService) {}

  ngOnInit() {
    this.groups = this.groupsService.getGroups();
    this.groupsService.groupsChange.subscribe((groups: Group[]) => {
      this.groups = groups;
    });
  }
}

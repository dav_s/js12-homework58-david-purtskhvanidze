import { Component, Input } from '@angular/core';
import { Group } from '../../shared/group.model';
import { GroupsService } from '../../shared/groups.service';

@Component({
  selector: 'app-group-item',
  templateUrl: './group-item.component.html',
  styleUrls: ['./group-item.component.css']
})
export class GroupItemComponent {
  @Input() group!: Group;

  constructor(private groupsService: GroupsService) {}

  onClick() {
    this.groupsService.addCurrentGroup(this.group);
  }
}

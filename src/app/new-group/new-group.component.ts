import { Component, ElementRef, ViewChild } from '@angular/core';
import { Group } from '../shared/group.model';
import { GroupsService } from '../shared/groups.service';

@Component({
  selector: 'app-new-group',
  templateUrl: './new-group.component.html',
  styleUrls: ['./new-group.component.css']
})
export class NewGroupComponent {

  @ViewChild('nameInput') nameInput!: ElementRef;

  constructor(public groupsService: GroupsService) {}

  createGroup() {
    const name: string = this.nameInput.nativeElement.value;
    const group = new Group(name, false, []);

    this.groupsService.addGroup(group);
  }

}

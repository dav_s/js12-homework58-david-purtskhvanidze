import { Component, Input } from '@angular/core';
import { User } from '../../shared/user.model';
import { GroupsService } from '../../shared/groups.service';

@Component({
  selector: 'app-user-item',
  templateUrl: './user-item.component.html',
  styleUrls: ['./user-item.component.css']
})
export class UserItemComponent {
  @Input() user!: User;

  constructor(private groupsService: GroupsService) {}

  onClick() {
    this.groupsService.addUserToGroup(this.user);
  }

}

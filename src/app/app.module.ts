import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { NewUserComponent } from './new-user/new-user.component';
import { NewGroupComponent } from './new-group/new-group.component';
import { UsersComponent } from './users/users.component';
import { GroupsComponent } from './groups/groups.component';
import { GroupItemComponent } from './groups/group-item/group-item.component';
import { UserItemComponent } from './users/user-item/user-item.component';
import { UsersService } from './shared/users.service';
import { GroupsService } from './shared/groups.service';

@NgModule({
  declarations: [
    AppComponent,
    NewUserComponent,
    NewGroupComponent,
    UsersComponent,
    GroupsComponent,
    GroupItemComponent,
    UserItemComponent
  ],
  imports: [
    BrowserModule,
    FormsModule
  ],
  providers: [
    UsersService,
    GroupsService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
